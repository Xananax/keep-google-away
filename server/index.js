const Gun = require('gun');

//...

app.use(Gun.serve);

const server = app.listen(port);

Gun({ file: 'db/data.json', web: server });