export type TokenType = 'TEXT'|'TITLE'|'CHECKBOX'|'BULLET'

export interface TokenProps{
  type:TokenType
  level?:number
  checked?:boolean
  content?:string|TokenProps
}

export interface TokenTitle extends TokenProps{ type:'TITLE', level:number }

const makeNeedle = (key:string) =>  new RegExp(`^|\.|\s(${key})\s|\.|$`,'gi')

const makeReplacer = (key:string, replacement:string ) => (str:string) => str.replace(makeNeedle(key), replacement)

const replacements = ([
  ['->','→'],
  ['<-','←'],
  ['=>','⇒'],
  ['<=','⇐'],
  [':)','☺'],
  [':(','☹'],
  ['<3','♥'],
  ['(c)','©'],
  ['(r)','®'],
  ['...','…'],
  ['(tm)','™']
] as [string,string][]).map(([key,replacement])=>{
  return makeReplacer(key,replacement)
})


const Token = (type:TokenType,content:string|TokenProps,level=0,checked=false):TokenProps => (
  {type,content,level,checked}
)

const getProps = (value:string):TokenProps => {

  if(/^\s*?#/.test(value)){
    const [, { length }, rendered ] = value.split(/(^#+?)([^#]*?)$/)
    return Token('TITLE',getProps(rendered),length)
  }
  else if(/^\s*?-/.test(value)){
    const [, {length}, rendered ] = value.split(/(^\s*?-)(.*?)$/)
    return Token('BULLET',getProps(rendered),length)
  }
  else if(/^\s*?\[.\]/.test(value)){
    const [, checked, rendered ] = value.split(/^\s*?\[(.)\](.*?)$/)
    const isChecked = !/\.|\s/.test(checked)
    return Token('CHECKBOX',getProps(rendered),0,isChecked)
  }
  return Token('TEXT', value)
}

class Line{
  _value:string=''
  _dirty:boolean=false
  set value(text:string){
    if(text!==this._value){
      this._dirty = true
      this._value = text
    }
  }
  render(){
    const props = getProps(this._value)
  }
  get tokens(){
    return ''
  }
}

export class Text{

}

import * as React from 'react'

const LineElement = ({value,onChange}:{value:string, onChange:(event: React.ChangeEvent<HTMLInputElement>) => void}) => 
  <input type='text' value={value} onChange={onChange}/>

class Line{
  value:string=''
  id:string=''
  index:number=0
  key:string=''
  trimmed:string=''
  rendered:string=''
  title:number=0
  bullet:number=0
  constructor(id:string, index:number){
    this.id = id
    this.key = id
    this.index = index
  }
  render(){
    const { value } = this;
    if(/^\s*?#/.test(value)){
      const [, title, rendered ] = value.split(/(^#+?)([^#]*?)$/)
      this.bullet = 0
      this.title = title.length
      this.trimmed = rendered
    }
    else if(/^\s*?-/.test(value)){
      const [, bullet, rendered ] = value.split(/(^\s*?-)(.*?)$/)
      this.title = 0
      this.bullet = bullet.length
      this.trimmed = rendered
    }else{
      this.title = 0
      this.bullet = 0
      this.trimmed = value
    }
    this.rendered = this.trimmed
  }
  get text(){
    this.render()
    return this.rendered
  }
}

export interface MapRay<T>{
  byId:{
    [id:string]:T
  },
  items:string[]
}

export interface InputState{
  lines:MapRay<Line>
}

export class Input extends React.Component<{},InputState>{

  state:InputState = {
    lines:{
      byId:{},
      items:[]
    }
  }

  index=0

  addLine = () => {
    const id = (this.index++)+'';
    const line = new Line(id, this.state.lines.items.length-1)
    const byId = { ...this.state.lines.byId, [id]:line}
    const items = [...this.state.lines.items, id ]
    const lines = { byId, items}
    this.setState({ lines })
  }

  removeLine = (id:string) => {
    const {[id]:_, ...byId} = this.state.lines.byId
    const items = this.state.lines.items.filter((_id)=>id!==_id)
    const lines = { byId, items }
    this.setState({ lines })
  }

  editLine = (id:string, props:Partial<Line> ) => {
    const { byId:previousById } = this.state.lines
    const byId = { ...previousById, [id]:({ ...previousById[id], ...props} as Line)}
    const lines:MapRay<Line> = { ...this.state.lines, byId }
    this.setState({ lines })
  }

  swapLines = (idx1:string, idx2:string) => {
    const items = this.state.lines.items.map((index)=>(
      index === idx1 ? idx2 : index === idx2 ? idx1 : index
    ))
    const lines = {...this.state.lines, items}
    this.setState({ lines })
  }

  onChange = (id:string) => (evt:React.ChangeEvent<HTMLInputElement>) => this.editLine(id, {value:evt.target.value})

  render(){
    const { items, byId } = this.state.lines
    return <div>
      { items.map( id => <LineElement {...byId[id]} onChange={this.onChange(id)}/>)}
    </div>
  }
}