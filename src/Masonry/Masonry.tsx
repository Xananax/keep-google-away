import * as React from "react";
import {Children} from "react";
import "./Masonry.css";

export interface MasonryColumnProps {
  children: React.ReactNode[];
}

export const MasonryColumn = ({children} : MasonryColumnProps) => (<div className="masonry-column">
  { children.map((child,i) => <div className="masonry-tile" key={i}>{child}</div>) }
</div>);

export const getColumnsCount = (width : number, breakPoints:number[]) => 
  ( breakPoints.reduceRight(
    ( previous, columnWidth, index ) => 
    ( columnWidth < width
    ? previous
    : index
    )
  , breakPoints.length
  ) + 1)

export const distributeInColumns = <Type extends {}>(columns_count:number, children:Type[]) => {
  const columns:Type[][]= [];
  children.forEach((child, key) => {
    const which = key % columns_count;
    columns[which] = columns[which] || []
    columns[which].push(child);
  });
  return columns;
}

export interface MasonryProps {
  breakPoints: number[];
  children: React.ReactNode;
  className?: string;
}

export const defaultBreaksPoints = [...((new Array(6)).keys())].map(k=>(k+1)*320)

export class Masonry extends React.Component<MasonryProps> {
  
  state = {
    columns_count: 1
  };

  static defaultProps: Partial<MasonryProps> = {
    breakPoints: defaultBreaksPoints
  };

  container: React.RefObject<HTMLDivElement> = React.createRef();

  componentDidMount() {
    this.onResize();
    window.addEventListener("resize", this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.onResize);
  }

  onResize = () => {
    const {container} = this;
    if (container === null || container.current === null) {
      return false;
    }
    const { offsetWidth } = container.current;
    const columns_count = getColumnsCount(offsetWidth,this.props.breakPoints);
    if (columns_count !== this.state.columns_count) {
      this.setState({ columns_count });
    }
  };

  distributeChildrenToColumns() {
    const {columns_count} = this.state;
    const children = Children.toArray(this.props.children)
    const columns = distributeInColumns(columns_count,children)
    return columns;
  }

  renderClassName(){
    const { className } = this.props
    return  (( className
      ? className + " "
      : "") + "masonry-container")
  }

  render() {
    const columns = this.distributeChildrenToColumns()
    const props = {
      className: this.renderClassName(),
      ref:this.container
    }
    return (<div {...props}> 
      {columns.map((col,i) => <MasonryColumn children={col} key={i}/>)}
    </div>);
  }
}
