import React, { Component } from 'react';
import { Masonry } from './Masonry'
import Gun from 'gun';
import SimpleMDE from 'react-simplemde-editor';
import "simplemde/dist/simplemde.min.css";
import './App.css';

const Note = ({text, id}) => <section id={`note-${id}`}>
  {text.split(`\n`).map((z,i)=><span key={i}>{z}<br/></span>)}
</section>

let images= [];
const imgId = [
  1011,
  883,
  1074,
  823,
  64,
  65,
  839,
  314,
  256,
  316,
  92,
  643
];

for (let i = 0; i < imgId.length; i++) {
  const ih = 200 + Math.floor(Math.random() * 10) * 15;
  images.push({text:"https://unsplash.it/250/" + ih + "?image=" + imgId[i], id:i, key:i+Math.random()});
}

class App extends Component {

  state = {
    currentText:'',
    notes:[
      { text:'hello world', id:'000',key:'sddd'},
      ...images
    ]
  }

  
  onSubmit = (evt) => {
    evt.preventDefault();
    this.addNote(this.state.currentText)
    this.setState({currentText:''})
  }
  onChange = (value) => this.setState({ currentText: value })

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <form onSubmit={this.onSubmit}>
            <SimpleMDE value={this.state.currentText} onChange={this.onChange} options={{autofocus:true,autosave:true,spellChecker:false}}/>
            <button>ok</button>
          </form>
        </header>
        <div className="notes">
          <Masonry>
            { this.state.notes.map( note => <Note {...note}/>)}
          </Masonry>
        </div>
      </div>
    );
  }
}

export default App;
